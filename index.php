<!-- CSV to Map converter -->
<!-- Made for Training Project Laboratory (VITMAL00) project at Budapest University of Technology and Economics in the 2017/2018/1 semester. -->
<!-- @author Komlós Boldizsár -->
<!-- @email komlos(dot)boldizsar(at)gmail(dot)com -->
<?php
$TABS = 2; // Count of tabulators used to tabulate embedded javascripts
require "convert_csv.php";
?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Maps</title>
    <link rel="stylesheet" href="style.css" type="text/css">
</head>
<body>
    <div id="map"></div>
    <div id="uploader">
        <h1>Upload CSV file</h1>
        <form action="" method="POST" enctype="multipart/form-data">
            <div class="file">
                <label for="csv">File:</label>
                <input type="file" name="csv" id="csv">
            </div>
            <div id="paths">
                <span class="warning">No path specified, please click on 'add path' and specify one!</span>
            </div>
            <div>
                <a href="javascript:;" onClick="add_path()">add path</a>
            </div>
            <div id="velocity_filter" class="setting">
                <input type="checkbox" id="use_velocity_filter" name="use_velocity_filter"<?php echo $use_velocity_filter ? " checked" : ""; ?>>
                <label for="use_velocity_filter">Use velocity filter:</label>
                <input type="number" min="0" name="velocity_filter" value="<?php echo $velocity_filter; ?>">
                * <?php echo $VELOCITY_FILTER_SCALER ?>
            </div>
            <div id="delimiter" class="setting">
                <label>CSV column delimiter:</label>
                <input type="text" name="delimiter" value="<?php echo $csv_delimiter; ?>">
            </div>
            <input type="submit" value="Upload">
        </form>
    </div>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBR3n5qWMTHlHgq6eBfp9nVuLF993rK-Lo&callback=initMap"></script>
    <script type="text/javascript" src="script.js"></script>
    <script type="text/javascript"><?php echo js_map_init(); ?></script>
    <script type="text/javascript"><?php echo js_fill_form(); ?></script>
</body>
</html>