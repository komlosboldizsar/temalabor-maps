<?php

/**
 * CSV to Map converter
 * Made for Training Project Laboratory (VITMAL00) project at Budapest University of Technology and Economics in the 2017/2018/1 semester.
 * @author Komlós Boldizsár <IGNO1V>
 * @email komlos.boldizsar@gmail.com
 */

// Default value for tabulation if value not set
if (!isset($TABS))
    $TABS = 0;

/* Templates */

/**
 * Template for initMap() javascript function.
 * Substitutions:
 *   %1$d: zoom value (see Google Maps API documentation for details)
 *   %2$f, %3$f: latitude and longitude coordinates of map center
 *   %4$s: path descriptor arrays (array of coordinates)
 *   %5$s: polyfill creator snippets
 */
$TEMPLATE_INITMAP = "
function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: %1\$d,
        center: {lat: %2\$f, lng: %3\$f},
        mapTypeId: 'terrain'
    });
    %4\$s%5\$s}\n";

/**
 * Template for an element of a path descriptor array (coordinate array).
 * Substitutions:
 *   %1$f: latitude coordinate
 *   %2$f: longitude coordinate
 */
$TEMPLATE_PATHOBJECT = "\n\t\t{ lat: %1\$f, lng: %2\$f }";

/**
 * Template for creating a path descriptor array (coordinate array) variable.
 * Substitutions:
 *   %1$d: index of the path (non-negative integer)
 *   %2$s: coordinate objects separated by comma
 */
$TEMPLATE_PATHARRAY = "var path%1\$d = [%2\$s\n\t];";

/**
 * Template of polyline creator snippet.
 * Substitutions:
 *   %1$d: index of the path (non-negative integer)
 *   %2$s: color of the polyline
 */
$TEMPLATE_POLYLINE = "
    var polyline%1\$d = new google.maps.Polyline({
        path: path%1\$d,
        geodesic: true,
        strokeColor: '%2\$s',
        strokeOpacity: 1.0,
        strokeWeight: 2
    });
    polyline%1\$d.setMap(map);\n";

/**
 * Template for a form filler method call (loads posted values to the form).
 * Substitutions:
 *   %1$f, %2$f: latitude and longitude column indices of the path configuration
 *   %3$s: color value of the path configuration
 */
$TEMPLATE_FORM_FILLER = "\n\tadd_path(%1\$f, %2\$f, '%3\$s');";

/**
 * Default delimiter used to separate columns in CSV files.
 * Comma is the most common, but semicolon is also used.
 */
$DEFAULT_DELIMITER = ",";

/**
 * Default value of the velocity filter.
 */
$DEFAULT_VELOCITY_FILTER = 100;

/**
 * Scaler for velocity filter, so user doesn't have to input values like "0.00000008".
 */
$VELOCITY_FILTER_SCALER = 0.0000000001;

/**
 * Use velocity filter by default?
 */
$DEFAULT_USE_VELOCITY_FILTER = false;

/**
 * Path to move the uploaded CSV file.
 */
$MOVE_FILE_TO = "uploaded.csv";

/**
 * Zoom value used for Google Maps if no file is uploaded and processed (no path to show).
 */
$ZOOM_IF_NO_FILE = 3;

/**
 * Zoom value used for Google Maps if file is uploaded and showing path(s) on the map.
 */
$ZOOM_IF_FILE_GIVEN = 17;

// Loading default values to parameter variables
$csv_delimiter = $DEFAULT_DELIMITER;
$velocity_filter = $DEFAULT_VELOCITY_FILTER;
$use_velocity_filter = $DEFAULT_USE_VELOCITY_FILTER;

// Process CSV file if uploaded
if (isset($_FILES['csv'])) {

    // Move temporary uploaded file to $MOVE_FILE_TO
    if ($_FILES['csv']['name'] != "") {
        $file_tmp = $_FILES['csv']['tmp_name'];
        move_uploaded_file($file_tmp, $MOVE_FILE_TO);
    }

    // Split CSV lines and columns
    $csv_contents = file_get_contents($MOVE_FILE_TO);
    $csv_lines = explode("\n", $csv_contents);
    $csv_delimiter = isset($_POST['delimiter']) ? $_POST['delimiter'] : $DEFAULT_DELIMITER;
    $csv_cells = array();
    foreach ($csv_lines as $csv_line)
        $csv_cells[] = explode($csv_delimiter, $csv_line);
    $csv_linecount = count($csv_lines);

    // Scale velocity filter if given
    if (isset($_POST['use_velocity_filter']) && $_POST['use_velocity_filter']) {
        $use_velocity_filter = true;
        $velocity_filter = $_POST['velocity_filter'];
    }
    $velocity_filter_scaled = $velocity_filter * $VELOCITY_FILTER_SCALER;

    // Initial values for minimal and maximal longitude and latitude variables for calculating ideal map center
    $min_lat = 180;
    $max_lat = -180;
    $min_lon = 180;
    $max_lon = -180;
    // Only used when velocity filter is active
    $column_timestamp = 0;

    // Arrays to put path descriptor objects and polyline creator snippets to
    $js_path_array_pieces = array();
    $js_polyline_pieces = array();
    // Process path configurations
    $path_index = 0;
    while (true) {

        // End of path configurations
        if (!isset($_POST['lat'][$path_index]))
            break;

        // Column indices in current configuration
        $column_lat = $_POST['lat'][$path_index];
        $column_lon = $_POST['lon'][$path_index];

        // Variables used only if velocity filter is active
        $first = true;
        $lat_prev = 0;
        $lon_prev = 0;
        $timestamp_prev = 0;

        // Process CSV line-by-line
        $path_objects = array();
        for ($line_number = 0; $line_number < $csv_linecount; $line_number++) {
            if ($csv_lines[$line_number] != "") {

                $timestamp = $csv_cells[$line_number][$column_timestamp];
                $lat = $csv_cells[$line_number][$column_lat];
                $lon = $csv_cells[$line_number][$column_lon];

                // 0,0 is probably invalid
                if (($lat != 0) && ($lon != 0)) {

                    $useable_values = true;

                    // Velocity filter calculations
                    if ($use_velocity_filter) {
                        $lat_diff = $lat - $lat_prev;
                        $lon_diff = $lon - $lon_prev;
                        $distance = sqrt($lat_diff * $lat_diff + $lon_diff * $lon_diff);
                        $timestamp_diff = $timestamp - $timestamp_prev;
                        $velocity = $distance / $timestamp_diff;
                        $useable_values = ($velocity < $velocity_filter_scaled);
                    }

                    // Add coordinates to array if values are not filtered by velocity
                    // or are probably valid
                    if (!$use_velocity_filter || $useable_values) {

                        $min_lat = min($min_lat, $lat);
                        $max_lat = max($max_lat, $lat);
                        $min_lon = min($min_lon, $lon);
                        $max_lon = max($max_lon, $lon);
                        $path_objects[] = sprintf($TEMPLATE_PATHOBJECT, $lat, $lon);

                        // Saving values for filtering
                        $first = false;
                        $lat_prev = $lat;
                        $lon_prev = $lon;
                        $timestamp_prev = $timestamp;

                    }

                }

            }
        }

        // Creating javascript snipptes from template
        $js_path_array_pieces[] = sprintf($TEMPLATE_PATHARRAY, $path_index, implode(",", $path_objects));
        $js_polyline_pieces[] = sprintf($TEMPLATE_POLYLINE, $path_index, $_POST['color'][$path_index]);

        $path_index++;

    }

};

/**
 * Create and return the initMap() javascript function
 * @return mixed Tabulated initMap() javascript snippet
 */
function js_map_init()
{
    global $ZOOM_IF_NO_FILE, $ZOOM_IF_FILE_GIVEN, $TEMPLATE_INITMAP, $min_lat, $max_lat, $min_lon, $max_lon;
    // Calculate map center
    if (isset($min_lat) && isset($max_lat) && isset($min_lon) && isset($max_lon)) {
        $avg_lat = ($min_lat + $max_lat) / 2;
        $avg_lon = ($min_lon + $max_lon) / 2;
        $zoom = $ZOOM_IF_FILE_GIVEN;
    } else {
        $avg_lat = 0;
        $avg_lon = 0;
        $zoom = $ZOOM_IF_NO_FILE;
    }
    return tabulate(sprintf($TEMPLATE_INITMAP, $zoom, $avg_lat, $avg_lon, js_path_arrays(), js_polylines()));
}

/**
 * Create and return javascript that creates the path descriptor arrays.
 * @return string Coordinate array definitions
 */
function js_path_arrays()
{
    global $js_path_array_pieces;
    if (isset($js_path_array_pieces))
        return implode("\n", $js_path_array_pieces);
    return "";
}

/**
 * Create and return javascripts that create the polylines.
 * @return string Polyline creator snippets
 */
function js_polylines()
{
    global $js_polyline_pieces;
    if (isset($js_polyline_pieces))
        return implode("\n", $js_polyline_pieces);
    return "";
}

/**
 * Create and return javascript method calls that fill the "Upload" form by POST data.
 * @return string Javascript snippet for filling the form with existing (posted) data
 */
function js_fill_form()
{
    global $TEMPLATE_FORM_FILLER;
    $fill_calls = array();
    $path_index = 0;
    while (true) {
        if (!isset($_POST['lat'][$path_index]))
            break;
        $fill_calls[] = sprintf($TEMPLATE_FORM_FILLER, $_POST['lat'][$path_index], $_POST['lon'][$path_index], $_POST['color'][$path_index]);
        $path_index++;
    }
    return tabulate(implode("", $fill_calls)) . "\n";
}

/**
 * Tabulate every row (separated by \n) of a string by $TABS tabulators
 * @param $str String to tabulate
 * @return mixed Tabulated string
 */
function tabulate($str)
{
    global $TABS;
    $tabs = str_repeat("\t", $TABS);
    return str_replace("\n", "\n" . $tabs, $str);
}

?>