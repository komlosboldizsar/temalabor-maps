// Color to use for path polylines
// When end of array is reached, picking a color is started from array start
var colors = [
    "#FF0000",
    "#3333FF",
    "#BB00FF",
    "#CC8800",
    "#008800"
];

/**
 * Add a path configuration row to the "Upload" box.
 * @param lat Latitude value to fill the latitude input with. Optional.
 * @param lon Longitude value to fill the latitude input with. Optional.
 * @param color Color value to fill the latitude input with. Optional.
 */
function add_path(lat, lon, color) {

    var paths = document.getElementById("paths");
    if (paths.childElementCount >= 10) {
        alert("ENOUGH!");
        return;
    }
    paths.getElementsByClassName("warning")[0].style.display = "none";

    var path = document.createElement("div");
    path.className = "path";

    var l1 = document.createElement("label");
    l1.textContent = "Column for latitude:";
    path.appendChild(l1);

    var i1 = document.createElement("input");
    i1.className = "lat";
    i1.name = "lat[]";
    i1.type = "number";
    i1.min = 0;
    i1.value = lat || "";
    path.appendChild(i1);

    var l2 = document.createElement("label");
    l2.textContent = "Column for longitude:";
    path.appendChild(l2);

    var i2 = document.createElement("input");
    i2.className = "lon";
    i2.name = "lon[]";
    i2.type = "number";
    i2.min = 0;
    i2.value = lon || "";
    path.appendChild(i2);

    var l3 = document.createElement("label");
    l3.textContent = "Color:";
    path.appendChild(l3);

    var i3 = document.createElement("input");
    i3.className = "color";
    i3.name = "color[]";
    i3.type = "text";
    i3.value = color || colors[paths.childElementCount % colors.length];
    path.appendChild(i3);

    var rem = document.createElement("a");
    rem.innerText = "remove";
    rem.href = "javascript:;";
    rem.onclick = function () {
        paths.removeChild(path);
    };
    path.appendChild(rem);

    paths.appendChild(path);

}